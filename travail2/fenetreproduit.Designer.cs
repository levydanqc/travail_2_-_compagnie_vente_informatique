﻿namespace Commerce
{
    partial class FenetreProduit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose ();
            }
            base.Dispose (disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.m_lblCode = new System.Windows.Forms.Label();
            this.m_txtCode = new System.Windows.Forms.TextBox();
            this.m_lblDescription = new System.Windows.Forms.Label();
            this.m_txtDescription = new System.Windows.Forms.TextBox();
            this.m_lblCout = new System.Windows.Forms.Label();
            this.m_btnAnnuler = new System.Windows.Forms.Button();
            this.m_btnOK = new System.Windows.Forms.Button();
            this.m_txtCout = new System.Windows.Forms.TextBox();
            this.m_grpCategorie = new System.Windows.Forms.GroupBox();
            this.m_optBoitiers = new System.Windows.Forms.RadioButton();
            this.m_optBlocsAlimentation = new System.Windows.Forms.RadioButton();
            this.m_optCartesVideo = new System.Windows.Forms.RadioButton();
            this.m_optUnitesStockage = new System.Windows.Forms.RadioButton();
            this.m_optMemoires = new System.Windows.Forms.RadioButton();
            this.m_optProcesseurs = new System.Windows.Forms.RadioButton();
            this.m_grpCategorie.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_lblCode
            // 
            this.m_lblCode.AutoSize = true;
            this.m_lblCode.Location = new System.Drawing.Point(12, 9);
            this.m_lblCode.Name = "m_lblCode";
            this.m_lblCode.Size = new System.Drawing.Size(35, 13);
            this.m_lblCode.TabIndex = 0;
            this.m_lblCode.Text = "&Code:";
            // 
            // m_txtCode
            // 
            this.m_txtCode.Location = new System.Drawing.Point(15, 25);
            this.m_txtCode.Name = "m_txtCode";
            this.m_txtCode.Size = new System.Drawing.Size(100, 20);
            this.m_txtCode.TabIndex = 1;
            // 
            // m_lblDescription
            // 
            this.m_lblDescription.AutoSize = true;
            this.m_lblDescription.Location = new System.Drawing.Point(12, 60);
            this.m_lblDescription.Name = "m_lblDescription";
            this.m_lblDescription.Size = new System.Drawing.Size(63, 13);
            this.m_lblDescription.TabIndex = 2;
            this.m_lblDescription.Text = "&Description:";
            // 
            // m_txtDescription
            // 
            this.m_txtDescription.Location = new System.Drawing.Point(15, 76);
            this.m_txtDescription.Name = "m_txtDescription";
            this.m_txtDescription.Size = new System.Drawing.Size(290, 20);
            this.m_txtDescription.TabIndex = 3;
            // 
            // m_lblCout
            // 
            this.m_lblCout.AutoSize = true;
            this.m_lblCout.Location = new System.Drawing.Point(12, 113);
            this.m_lblCout.Name = "m_lblCout";
            this.m_lblCout.Size = new System.Drawing.Size(32, 13);
            this.m_lblCout.TabIndex = 4;
            this.m_lblCout.Text = "Coû&t:";
            // 
            // m_btnAnnuler
            // 
            this.m_btnAnnuler.Location = new System.Drawing.Point(272, 206);
            this.m_btnAnnuler.Name = "m_btnAnnuler";
            this.m_btnAnnuler.Size = new System.Drawing.Size(75, 23);
            this.m_btnAnnuler.TabIndex = 8;
            this.m_btnAnnuler.Text = "A&nnuler";
            this.m_btnAnnuler.UseVisualStyleBackColor = true;
            // 
            // m_btnOK
            // 
            this.m_btnOK.Location = new System.Drawing.Point(191, 206);
            this.m_btnOK.Name = "m_btnOK";
            this.m_btnOK.Size = new System.Drawing.Size(75, 23);
            this.m_btnOK.TabIndex = 7;
            this.m_btnOK.Text = "&OK";
            this.m_btnOK.UseVisualStyleBackColor = true;
            this.m_btnOK.Click += new System.EventHandler(this.m_btnOK_Click);
            // 
            // m_txtCout
            // 
            this.m_txtCout.Location = new System.Drawing.Point(15, 129);
            this.m_txtCout.Name = "m_txtCout";
            this.m_txtCout.Size = new System.Drawing.Size(100, 20);
            this.m_txtCout.TabIndex = 5;
            // 
            // m_grpCategorie
            // 
            this.m_grpCategorie.Controls.Add(this.m_optBoitiers);
            this.m_grpCategorie.Controls.Add(this.m_optBlocsAlimentation);
            this.m_grpCategorie.Controls.Add(this.m_optCartesVideo);
            this.m_grpCategorie.Controls.Add(this.m_optUnitesStockage);
            this.m_grpCategorie.Controls.Add(this.m_optMemoires);
            this.m_grpCategorie.Controls.Add(this.m_optProcesseurs);
            this.m_grpCategorie.Location = new System.Drawing.Point(324, 25);
            this.m_grpCategorie.Name = "m_grpCategorie";
            this.m_grpCategorie.Size = new System.Drawing.Size(200, 165);
            this.m_grpCategorie.TabIndex = 6;
            this.m_grpCategorie.TabStop = false;
            this.m_grpCategorie.Text = "C&atégorie:";
            // 
            // m_optBoitiers
            // 
            this.m_optBoitiers.AutoSize = true;
            this.m_optBoitiers.Location = new System.Drawing.Point(6, 134);
            this.m_optBoitiers.Name = "m_optBoitiers";
            this.m_optBoitiers.Size = new System.Drawing.Size(59, 17);
            this.m_optBoitiers.TabIndex = 5;
            this.m_optBoitiers.TabStop = true;
            this.m_optBoitiers.Text = "Bo&itiers";
            this.m_optBoitiers.UseVisualStyleBackColor = true;
            // 
            // m_optBlocsAlimentation
            // 
            this.m_optBlocsAlimentation.AutoSize = true;
            this.m_optBlocsAlimentation.Location = new System.Drawing.Point(6, 111);
            this.m_optBlocsAlimentation.Name = "m_optBlocsAlimentation";
            this.m_optBlocsAlimentation.Size = new System.Drawing.Size(118, 17);
            this.m_optBlocsAlimentation.TabIndex = 4;
            this.m_optBlocsAlimentation.TabStop = true;
            this.m_optBlocsAlimentation.Text = "&Blocs d\'alimentation";
            this.m_optBlocsAlimentation.UseVisualStyleBackColor = true;
            // 
            // m_optCartesVideo
            // 
            this.m_optCartesVideo.AutoSize = true;
            this.m_optCartesVideo.Location = new System.Drawing.Point(6, 88);
            this.m_optCartesVideo.Name = "m_optCartesVideo";
            this.m_optCartesVideo.Size = new System.Drawing.Size(85, 17);
            this.m_optCartesVideo.TabIndex = 3;
            this.m_optCartesVideo.TabStop = true;
            this.m_optCartesVideo.Text = "Cartes &Vidéo";
            this.m_optCartesVideo.UseVisualStyleBackColor = true;
            // 
            // m_optUnitesStockage
            // 
            this.m_optUnitesStockage.AutoSize = true;
            this.m_optUnitesStockage.Location = new System.Drawing.Point(6, 65);
            this.m_optUnitesStockage.Name = "m_optUnitesStockage";
            this.m_optUnitesStockage.Size = new System.Drawing.Size(117, 17);
            this.m_optUnitesStockage.TabIndex = 2;
            this.m_optUnitesStockage.TabStop = true;
            this.m_optUnitesStockage.Text = "&Unités de stockage";
            this.m_optUnitesStockage.UseVisualStyleBackColor = true;
            // 
            // m_optMemoires
            // 
            this.m_optMemoires.AutoSize = true;
            this.m_optMemoires.Location = new System.Drawing.Point(6, 42);
            this.m_optMemoires.Name = "m_optMemoires";
            this.m_optMemoires.Size = new System.Drawing.Size(70, 17);
            this.m_optMemoires.TabIndex = 1;
            this.m_optMemoires.TabStop = true;
            this.m_optMemoires.Text = "&Mémoires";
            this.m_optMemoires.UseVisualStyleBackColor = true;
            // 
            // m_optProcesseurs
            // 
            this.m_optProcesseurs.AutoSize = true;
            this.m_optProcesseurs.Location = new System.Drawing.Point(6, 19);
            this.m_optProcesseurs.Name = "m_optProcesseurs";
            this.m_optProcesseurs.Size = new System.Drawing.Size(83, 17);
            this.m_optProcesseurs.TabIndex = 0;
            this.m_optProcesseurs.TabStop = true;
            this.m_optProcesseurs.Text = "&Processeurs";
            this.m_optProcesseurs.UseVisualStyleBackColor = true;
            // 
            // FenetreProduit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 242);
            this.Controls.Add(this.m_grpCategorie);
            this.Controls.Add(this.m_txtCout);
            this.Controls.Add(this.m_btnAnnuler);
            this.Controls.Add(this.m_btnOK);
            this.Controls.Add(this.m_lblCout);
            this.Controls.Add(this.m_txtDescription);
            this.Controls.Add(this.m_lblDescription);
            this.Controls.Add(this.m_txtCode);
            this.Controls.Add(this.m_lblCode);
            this.Name = "FenetreProduit";
            this.Text = "fenetreproduit";
            this.Load += new System.EventHandler(this.FenetreProduit_Load);
            this.m_grpCategorie.ResumeLayout(false);
            this.m_grpCategorie.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label m_lblCode;
        private System.Windows.Forms.TextBox m_txtCode;
        private System.Windows.Forms.Label m_lblDescription;
        private System.Windows.Forms.TextBox m_txtDescription;
        private System.Windows.Forms.Label m_lblCout;
        private System.Windows.Forms.TextBox m_txtCout;        
        private System.Windows.Forms.GroupBox m_grpCategorie;
        private System.Windows.Forms.RadioButton m_optProcesseurs;
        private System.Windows.Forms.RadioButton m_optMemoires;
        private System.Windows.Forms.RadioButton m_optUnitesStockage;
        private System.Windows.Forms.RadioButton m_optCartesVideo;
        private System.Windows.Forms.RadioButton m_optBlocsAlimentation;
        private System.Windows.Forms.RadioButton m_optBoitiers;
        private System.Windows.Forms.Button m_btnOK;
        private System.Windows.Forms.Button m_btnAnnuler;
    }
}