﻿/******************************************************************************
 * Fichier      : Inventaire.cs
 * 
 * Classe       : Inventaire
 *
 * Description  : Classe d'un inventaire contenant la liste des stocks.
 * 
 * Auteur       : Dan Lévy
 * 
 * GitLab       : gitlab.com/levydanqc/travail_2_-_compagnie_vente_informatique
 ******************************************************************************/

using System;
using System.Collections.Generic;

namespace Commerce
{
    /// <summary>
    /// Classe représentant un inventaire.
    /// </summary>
    public class Inventaire
    {
        private List<Stock> m_lesStocks;

        /// <summary>
        /// Constructeur de la classe inventaire.
        /// </summary>
        public Inventaire()
        {
            this.m_lesStocks = new List<Stock>();
        }

        /// <summary>
        /// Accesseur en lecture du nombre de stock dans l'inventaire.
        /// </summary>
        public int NbStocks
        {
            get { return m_lesStocks.Count; }
        }

        /// <summary>
        /// Permet d'obtenir un stock dans l'inventaire.
        /// </summary>
        /// <param name="iIndice">Indice du stock.</param>
        /// <returns>Le stock dans l'inventaire à l'indice.</returns>
        public Stock GetStock(int iIndice)
        {
            try
            {
                return m_lesStocks[iIndice];
            }
            catch
            {
                throw new IndexOutOfRangeException
                    ("L'indice est en dehors de la plage.");
            }
        }

        /// <summary>
        /// Permet d'ajouter un stock à l'inventaire.
        /// </summary>
        /// <param name="leStock">Le stock à ajouter.</param>
        /// <returns>true si le stock n'est pas déjà dans l'inventaire,
        /// false autrement.</returns>
        public bool Ajouter(in Stock leStock)
        {
            if (!m_lesStocks.Contains(leStock) && leStock != null)
            {
                m_lesStocks.Add(leStock);
                return true;
            }
            else return false;
        }

        /// <summary>
        /// Permet de retirer un stock de l'inventaire.
        /// </summary>
        /// <param name="leStock">Un stock à retirer.</param>
        /// <returns>true si le stock est bien supprimé,
        /// false autrement.</returns>
        public bool Retirer(in Stock leStock)
        {
            return m_lesStocks.Remove(leStock);
        }
    }
}
