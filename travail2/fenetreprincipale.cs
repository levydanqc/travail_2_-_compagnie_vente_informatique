﻿/******************************************************************************
 * Fichier      : fenetreprincipale.cs
 * 
 * Classe       : FenetrePrincipale (dérivé de Form)
 *
 * Description  : Fenêtre principale du programme.
 * 
 * Auteur       : Dan Lévy
 * 
 * GitLab       : gitlab.com/levydanqc/travail_2_-_compagnie_vente_informatique
 ******************************************************************************/

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Utilitaires;
namespace Commerce
{
    /// <summary>
    /// Classe représentant une fenêtre principale.
    /// </summary>
    public partial class FenetrePrincipale : Form
    {
        // Inventaire total de tous les produits dans toutes les succursales
        private Inventaire m_leInventaire;
        // Tous les codes des produits existants dans la compagnie
        private List<int> m_iLesCodes;
        // Montant total des ventes de la compagnie
        private decimal m_mTotalVentes;
        // Montant total des achats de la compagnie
        private decimal m_mTotalAchats;

        // Texte qui doit apparaitre dans la barre-titre de la fenêtre
        // principale et dans la barre-titre de chacune des boites de
        // message.
        public const string TITRE_APPLICATION = "Travail 2";

        // Nombre qui représente le code de produit dans la numérotation des
        // produits (PREMIER_CODE_PRODUIT, PREMIER_CODE_PRODUIT + 1,
        // PREMIER_CODE_PRODUIT + 2, PREMIER_CODE_PRODUIT + 3, etc.).
        private const int PREMIER_CODE_PRODUIT = 1001;

        // Nombre qui représente la quantité maximale qu'une succursale peut
        // avoir en inventaire.
        private const int MAX_QUANTITE_PRODUITS = 10;

        // Nombre qui sert à déterminer le pourcentage de profit (par rapport
        // au coût d'achat) que la compagnie fera lors de la vente d'un
        // produit.
        private const decimal MARGE_BENEFICIAIRE = 0.2m;

        /// <summary>
        /// Constructeur de la fenêtre principale.
        /// </summary>
        public FenetrePrincipale()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            Text = TITRE_APPLICATION;
            m_lstSuccursales.Sorted = true;
            m_lstProduits.Sorted = true;

            // À FAIRE:
            // Initialiser les attributs qui doivent l'être ici.
            m_iLesCodes = new List<int> { PREMIER_CODE_PRODUIT };
            m_leInventaire = new Inventaire();
            m_mTotalAchats = 0;
            m_mTotalVentes = 0;
        }

        private void FenetrePrincipale_Load(object sender, System.EventArgs e)
        {
        }

        /// <summary>
        /// Clique dans la liste des succursale.
        /// Déselectionne l'item dans la liste lorsqu'on clique dans le vide.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_lstSuccursales_MouseClick(object sender,
            MouseEventArgs e)
        {
            int iIndice;

            iIndice = m_lstSuccursales.IndexFromPoint(e.Location);
            if (iIndice == -1)
            {
                m_lstSuccursales.SelectedItem = null;
            }
        }

        /// <summary>
        /// Double-clique dans la liste des succursales.
        /// Double clique est équivalent au bouton 'Modifier Succursale'.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_lstSuccursales_MouseDoubleClick(object sender,
            MouseEventArgs e)
        {
            int iIndice;

            iIndice = m_lstSuccursales.IndexFromPoint(e.Location);
            if (iIndice != -1)
            {
                m_lstSuccursales.SelectedIndex = iIndice;
                this.m_btnModifierSuccursale_Click(null, null);
            }
        }

        /// <summary>
        /// Clique dans la liste des produits.
        /// Déselectionne l'item dans la liste lorsqu'on clique dans le vide.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_lstProduits_MouseClick(object sender, MouseEventArgs e)
        {
            int iIndice;

            iIndice = m_lstProduits.IndexFromPoint(e.Location);
            if (iIndice == -1)
            {
                m_lstProduits.SelectedItem = null;
            }
        }

        /// <summary>
        /// Double-clique dans la liste des produits.
        /// Double clique est équivalent au bouton 'Modifier Produit'.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_lstProduits_MouseDoubleClick(object sender,
            MouseEventArgs e)
        {
            int iIndice;

            iIndice = m_lstProduits.IndexFromPoint(e.Location);
            if (iIndice != -1)
            {
                m_lstProduits.SelectedIndex = iIndice;
                this.m_btnModifierProduit_Click(null, null);
            }
        }
        /// <summary>
        /// Bouton 'Ajouter Succursale'.
        /// Permet de créer une succursale.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_btnAjouterSuccursale_Click(object sender,
            System.EventArgs e)
        {
            FenetreSuccursale frmAjoutSuccursale;
            int iIndice;
            frmAjoutSuccursale = new FenetreSuccursale();

            frmAjoutSuccursale.Text = TITRE_APPLICATION;

            if (frmAjoutSuccursale.ShowDialog() == DialogResult.OK)
            {
                Succursale laSuccursale = frmAjoutSuccursale.LaSuccursale;

                if (m_lstSuccursales.Items.Contains(laSuccursale))
                {
                    MessageBox.Show
                        ("Une succursale existe déjà à cette adresse.",
                        TITRE_APPLICATION,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
                else
                {
                    // S'il y a au moins 1 produit dans la liste
                    if (m_lstProduits.Items.Count > 0)
                    {
                        foreach (Produit leProduit in m_lstProduits.Items)
                        {
                            m_leInventaire.Ajouter(
                                new Stock(laSuccursale, leProduit));
                        }
                    }
                    iIndice = m_lstSuccursales.Items.Add(laSuccursale);
                    m_lstSuccursales.SelectedIndex = iIndice;
                }
            }

            frmAjoutSuccursale.Dispose();
        }

        /// <summary>
        /// Bouton 'Modifier Succursale'.
        /// Permet de modifier le numéro de téléphone d'une succursale.
        /// </summary
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_btnModifierSuccursale_Click(object sender,
            System.EventArgs e)
        {
            Succursale uneSuccursale;
            FenetreSuccursale frmAjoutSuccursale;

            uneSuccursale = (Succursale)m_lstSuccursales.SelectedItem;

            if (uneSuccursale == null)
            {
                MessageBox.Show(
                    "Vous devez sélectionner une succursale pour pouvoir la" +
                    " modifier.",
                    TITRE_APPLICATION,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
            else
            {
                frmAjoutSuccursale = new FenetreSuccursale();
                frmAjoutSuccursale.LaSuccursale = uneSuccursale;

                if (frmAjoutSuccursale.ShowDialog() == DialogResult.OK)
                {
                    int iIndice;
                    iIndice = m_lstSuccursales.Items.IndexOf(uneSuccursale);
                    m_lstSuccursales.Items.Remove(uneSuccursale);
                    m_lstSuccursales.Items.Insert(iIndice,
                        frmAjoutSuccursale.LaSuccursale);
                    m_lstSuccursales.SelectedIndex = iIndice;
                }

                frmAjoutSuccursale.Dispose();
            }
        }

        /// <summary>
        /// Bouton 'Supprimer Succursale'.
        /// Permet de supprimer une succursale de la liste et de vendre tout
        /// ses stocks.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_btnSupprimerSuccursale_Click(object sender,
            System.EventArgs e)
        {
            Succursale uneSuccursale;
            Stock unStock;
            int iIndex;
            iIndex = m_lstSuccursales.SelectedIndex;
            uneSuccursale = (Succursale)m_lstSuccursales.SelectedItem;

            if (uneSuccursale == null) // Aucune succursale sélectionnée
            {
                MessageBox.Show(
                    "Vous devez sélectionner une succursale pour pouvoir la" +
                    " supprimer.",
                    TITRE_APPLICATION,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
            else
            {
                for (int i = 0; i < m_leInventaire.NbStocks; i++)
                {
                    unStock = m_leInventaire.GetStock(i);

                    if (unStock.Succursale == uneSuccursale)
                    {
                        m_mTotalVentes += unStock.Produit.Cout * 
                            (1 + MARGE_BENEFICIAIRE) * unStock.Quantite;
                        m_leInventaire.Retirer(unStock);
                    }
                }

                m_lstSuccursales.Items.RemoveAt(iIndex);
                // Sélectionne le prochain élément de la liste
                // ou le précédant si le dernier est supprimé
                m_lstSuccursales.SelectedIndex = iIndex - 
                    Convert.ToInt32(iIndex == m_lstSuccursales.Items.Count);

            }
        }

        /// <summary>
        /// Bouton 'Ajouter Produit'.
        /// Permet d'ajouter un produit à la liste des produits.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_btnAjouterProduit_Click(object sender, EventArgs e)
        {
            FenetreProduit frmAjoutProduit;
            int iIndice;
            frmAjoutProduit = new FenetreProduit();
            int iCode = 0;
            int iActuel;
            frmAjoutProduit.Text = TITRE_APPLICATION;

            /* Trouver le code du produit dans la liste
             * Le code est soit :
             * le premier (1001)
             * le prochain (dernier + 1)
             * le premier manquant
             */
            bool bTrouve = false;
            // Premier de la liste
            if (m_iLesCodes[0] != PREMIER_CODE_PRODUIT || 
                m_lstProduits.Items.Count == 0)
            {
                iCode = PREMIER_CODE_PRODUIT;
            }
            // Dernier de la liste
            else
            if (m_iLesCodes[m_iLesCodes.Count - 1] - 1000 == m_iLesCodes.Count)
            {
                iCode = m_iLesCodes[m_iLesCodes.Count - 1] + 1;
            }
            // Premier manquant
            else
            {
                int i = 1;
                while (!bTrouve && i < m_iLesCodes.Count)
                {
                    iActuel = m_iLesCodes[i];
                    iCode = m_iLesCodes[i - 1] + 1;

                    if (iActuel != iCode)
                    {
                        bTrouve = true;
                    }

                    i++;
                }
            }
            m_iLesCodes.Insert(iCode - PREMIER_CODE_PRODUIT, iCode);
            frmAjoutProduit.LeCode = iCode;

            if (frmAjoutProduit.ShowDialog() == DialogResult.OK)
            {
                Produit leProduit = frmAjoutProduit.LeProduit;

                if (m_lstSuccursales.Items.Count > 0)
                {
                    foreach (Succursale laSuccursale in m_lstSuccursales.Items)
                    {
                        m_leInventaire.Ajouter(
                            new Stock(laSuccursale, leProduit));
                    }
                }

                iIndice = m_lstProduits.Items.Add(leProduit);
                m_lstProduits.SelectedIndex = iIndice;
            }

            frmAjoutProduit.Dispose();
        }

        /// <summary>
        /// Bouton 'Modifier Produit'.
        /// Permet de modifier la description, le coût et le type d'un produit.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_btnModifierProduit_Click(object sender, EventArgs e)
        {
            Produit unProduit;
            FenetreProduit frmAjoutProduit;

            unProduit = (Produit)m_lstProduits.SelectedItem;

            if (unProduit == null)
            {
                MessageBox.Show(
                    "Vous devez sélectionner un produit pour pouvoir le" +
                    " modifier.",
                    TITRE_APPLICATION,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
            else
            {
                frmAjoutProduit = new FenetreProduit();
                frmAjoutProduit.LeProduit = unProduit;

                if (frmAjoutProduit.ShowDialog() == DialogResult.OK)
                {
                    int iIndice;
                    iIndice = m_lstProduits.Items.IndexOf(unProduit);
                    m_lstProduits.Items.Remove(unProduit);
                    m_lstProduits.Items.Insert(
                        iIndice, frmAjoutProduit.LeProduit);
                    m_lstProduits.SelectedIndex = iIndice;
                }

                frmAjoutProduit.Dispose();
            }
        }

        /// <summary>
        /// Bouton 'Supprimer Produit'.
        /// Permet de supprimer un produit de la liste des produits et de
        /// vendre ses stocks.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_btnSupprimerProduit_Click(object sender, EventArgs e)
        {
            Produit unProduit;
            Stock unStock;
            int iIndex;
            iIndex = m_lstProduits.SelectedIndex;
            unProduit = (Produit)m_lstProduits.SelectedItem;

            if (unProduit == null) // Aucun produit sélectionné
            {
                MessageBox.Show(
                    "Vous devez sélectionner un produit pour pouvoir le " +
                    "supprimer.",
                    TITRE_APPLICATION,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
            else
            {
                for (int i = 0; i < m_leInventaire.NbStocks; i++)
                {
                    unStock = m_leInventaire.GetStock(i);

                    if (unStock.Produit == unProduit)
                    {
                        m_mTotalVentes += unStock.Produit.Cout *
                            (1 + MARGE_BENEFICIAIRE) * unStock.Quantite;
                        m_leInventaire.Retirer(unStock);
                    }
                }

                m_iLesCodes.Remove(int.Parse(unProduit.Code));

                m_lstProduits.Items.RemoveAt(iIndex);
                // Sélectionne le prochain élément de la liste
                // ou le précédant si le dernier est supprimé
                m_lstProduits.SelectedIndex = iIndex - 
                    Convert.ToInt32(iIndex == m_lstProduits.Items.Count);
            }
        }

        /// <summary>
        /// Bouton 'Commander Produits'.
        /// Permet de réapprovisionner les succursales de leurs quantités
        /// maximales de chaque produit.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_btnCommanderProduits_Click(object sender, EventArgs e)
        {
            Stock unStock;
            int iQtTotalStock = 0;

            if (m_leInventaire.NbStocks > 0)
            {
                for (int i = 0; i < m_leInventaire.NbStocks; i++)
                {
                    unStock = m_leInventaire.GetStock(i);
                    iQtTotalStock += (int)unStock.Quantite;

                    m_mTotalAchats += unStock.Produit.Cout * 
                        (MAX_QUANTITE_PRODUITS - unStock.Quantite);
                    unStock.Quantite = MAX_QUANTITE_PRODUITS;
                }
                if (iQtTotalStock == m_leInventaire.NbStocks *
                    MAX_QUANTITE_PRODUITS)
                { // Tout les stocks sont pleins
                    MessageBox.Show(
                        "Tous les stocks sont à la capacité maximale.",
                        TITRE_APPLICATION,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show(
                    "Il est impossible de commander des produits sans stocks.",
                    TITRE_APPLICATION,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Bouton 'Generer Ventes'.
        /// Permet de générer des ventes aléatoires.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_btnGenererVentes_Click(object sender, EventArgs e)
        {
            int iQtVente;
            Stock unStock;
            int iQtTotalStock = 0;

            if (m_leInventaire.NbStocks > 0 && m_mTotalAchats > 0)
            {
                for (int i = 0; i < m_leInventaire.NbStocks; i++)
                {
                    unStock = m_leInventaire.GetStock(i);
                    iQtTotalStock += (int)unStock.Quantite;
                    iQtVente = Aleatoire.GenererNombre((int)unStock.Quantite);
                    
                    m_mTotalVentes += unStock.Produit.Cout *
                        (1 + MARGE_BENEFICIAIRE) * iQtVente;

                    unStock.Quantite -= (uint)iQtVente;
                }
                if (iQtTotalStock == 0) // Tous les stocks sont vides
                {
                    MessageBox.Show(
                        "Il n'y a plus aucun produit à vendre.",
                        TITRE_APPLICATION,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show(
                    "Il est impossible de générer des ventes sans stocks.",
                    TITRE_APPLICATION,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Bouton 'Bilan'.
        /// Permet d'afficher le bilan total d'achats et de ventes
        /// de la compagnie.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_btnBilan_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                String.Format(
                    "Recettes: {0:c}\nAchats: {1:c}.", m_mTotalVentes,
                    m_mTotalAchats),
                TITRE_APPLICATION,
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        /// <summary>
        /// Bouton 'A propos'.
        /// Permet d'afficher les informations du créateurs de l'application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_btnAPropos_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                String.Format(
                    "{0} réalisé par {1} {2}.", TITRE_APPLICATION,
                    "Dan Lévy", "1861154"),
                TITRE_APPLICATION,
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }
    }
}