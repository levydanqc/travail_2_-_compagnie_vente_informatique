﻿/******************************************************************************
 * Fichier      : Succursale.cs
 * 
 * Classe       : Succursale
 *
 * Description  : Classe d'une succursale.
 * 
 * Auteur       : Dan Lévy
 * 
 * GitLab       : gitlab.com/levydanqc/travail_2_-_compagnie_vente_informatique
 ******************************************************************************/

using System;
using System.Linq;

namespace Commerce
{
    /// <summary>
    /// Classe représentant une succursale.
    /// </summary>
    public class Succursale
    {
        private string m_strAdresse;
        private string m_strTelephone;

        /// <summary>
        /// Constructeur d'une succursale.
        /// </summary>
        /// <param name="strAdresse">Adresse de la succursale.</param>
        /// <param name="strTelephone">Numéro de téléphone de la
        /// succursale.</param>
        public Succursale(string strAdresse, string strTelephone)
        {
            if (strAdresse == "")
            {
                throw new ArgumentException("L'adresse est vide.");
            }
            this.m_strAdresse = strAdresse;
            this.Telephone = strTelephone;
        }

        /// <summary>
        /// Accesseur en lecture de l'adresse.
        /// </summary>
        public string Adresse
        {
            get { return m_strAdresse; }
        }

        /// <summary>
        /// Accesseur en écriture et en lecture de numéro de téléphone.
        /// </summary>
        public string Telephone
        {
            get { return m_strTelephone; }
            set
            {
                if (value.Length != 10)
                {
                    throw new ArgumentException
                        ("La longueur du numéro de téléphone est invalide.");
                }
                if (!new[] { "418", "514" }.Contains(value.Substring(0, 3)))
                {
                    throw new ArgumentException
                        ("Le code régional est invalide.");
                }
                foreach (char number in value.Substring(3))
                {
                    if (number < '0' || number > '9')
                    {
                        throw new ArgumentException
                            ("Le numéro de téléphone contient au moins un " +
                            "caractère qui n'est pas un chiffre.");
                    }
                }
                this.m_strTelephone = value;
            }
        }

        /// <summary>
        /// Réécriture de la méthode ToString()
        /// </summary>
        /// <returns>Une chaine de caractères contenant l'adresse et le numéro
        /// de téléphone</returns>
        public override string ToString()
        {
            return String.Format("{0} ({1}-{2}-{3})",
                m_strAdresse,
                m_strTelephone.Substring(0, 3),
                m_strTelephone.Substring(3, 3),
                m_strTelephone.Substring(6));
        }

        /// <summary>
        /// Réécriture de l'opérateur d'égalité.
        /// </summary>
        /// <param name="succursale1">Une succursales à comparer.</param>
        /// <param name="succursale2">Une succursales à comparer.</param>
        /// <returns>true si les adresses des succursales sont identiques,
        /// false autrement.</returns>
        public static bool operator == (in Succursale succursale1, 
            in Succursale succursale2)
        {
            if (Object.ReferenceEquals(succursale1, succursale2))
            {
                return true;
            }
            if ((Object)succursale1 == null
               || (Object)succursale2 == null)
            {
                return false;
            }
            return (succursale1.m_strAdresse == succursale2.m_strAdresse);
        }

        /// <summary>
        /// Réécriture de l'opérateur d'inégalité.
        /// </summary>
        /// <param name="succursale1">Une succursales à comparer.</param>
        /// <param name="succursale2">Une succursales à comparer.</param>
        /// <returns>true si les adresses sont différentes,
        /// false autrement.</returns>
        public static bool operator != (in Succursale succursale1,
            in Succursale succursale2)
        {
            return !(succursale1 == succursale2);
        }

        public override bool Equals(object obj)
        {
            if (obj is Succursale)
            {
                return (this == (Succursale)obj);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }
}
