﻿/******************************************************************************
 * Fichier      : fenetresuccursale.cs
 * 
 * Classe       : FenetreSuccursale (dérivé de Form)
 *
 * Description  : Fenêtre d'ajout et de modification d'une succursale.
 * 
 * Auteur       : Dan Lévy
 * 
 * GitLab       : gitlab.com/levydanqc/travail_2_-_compagnie_vente_informatique
 ******************************************************************************/

using System.Windows.Forms;

namespace Commerce
{
    /// <summary>
    /// Classe représentant une fenêtre d'ajout d'une succursale.
    /// </summary>
    public partial class FenetreSuccursale : Form
    {
        // Succursale à ajouter ou à modifier
        private Succursale m_laSuccursale;
        
        /// <summary>
        /// Constructeur d'une fenetre d'ajout de succursale.
        /// </summary>
        public FenetreSuccursale()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MinimizeBox = false;
            MaximizeBox = false;
            ShowInTaskbar = false;
            AcceptButton = m_btnOK;
            CancelButton = m_btnAnnuler;
            m_btnOK.DialogResult = DialogResult.OK;
            m_btnAnnuler.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Accesseur en lecture et en écriture de la succursale.
        /// </summary>
        public Succursale LaSuccursale
        {
            get { return m_laSuccursale; }
            set
            {
                m_laSuccursale = value;
                m_txtAdresse.Text = value.Adresse;
                m_txtNumeroTelephone.Text = value.Telephone;
                m_txtAdresse.ReadOnly = true;
                m_txtAdresse.TabStop = false;
            }
        }

        private void FenetreSuccursale_Load(object sender, System.EventArgs e)
        {

        }

        /// <summary>
        /// Bouton OK.
        /// Permet de créer ou modifier une succursale.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_btnOK_Click(object sender, System.EventArgs e)
        {
            try
            {
                this.LaSuccursale = new Succursale(m_txtAdresse.Text,
                    m_txtNumeroTelephone.Text);
            }
            catch (System.ArgumentException uneException)
            {

                MessageBox.Show
                    (uneException.Message, FenetrePrincipale.TITRE_APPLICATION,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);


                if (uneException.Message == "L'adresse est vide.")
                {
                    m_txtAdresse.Focus();
                }
                else
                {
                    m_txtNumeroTelephone.Focus();
                }

                DialogResult = DialogResult.None;

            }
        }
    }
}
