﻿namespace Commerce
{
    partial class FenetreSuccursale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose ();
            }
            base.Dispose (disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.m_lblAdresse = new System.Windows.Forms.Label();
            this.m_txtAdresse = new System.Windows.Forms.TextBox();
            this.m_lblNumeroTelephone = new System.Windows.Forms.Label();
            this.m_txtNumeroTelephone = new System.Windows.Forms.TextBox();
            this.m_btnOK = new System.Windows.Forms.Button();
            this.m_btnAnnuler = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_lblAdresse
            // 
            this.m_lblAdresse.AutoSize = true;
            this.m_lblAdresse.Location = new System.Drawing.Point(9, 9);
            this.m_lblAdresse.Name = "m_lblAdresse";
            this.m_lblAdresse.Size = new System.Drawing.Size(48, 13);
            this.m_lblAdresse.TabIndex = 0;
            this.m_lblAdresse.Text = "&Adresse:";
            // 
            // m_txtAdresse
            // 
            this.m_txtAdresse.Location = new System.Drawing.Point(12, 25);
            this.m_txtAdresse.Name = "m_txtAdresse";
            this.m_txtAdresse.Size = new System.Drawing.Size(334, 20);
            this.m_txtAdresse.TabIndex = 1;
            // 
            // m_lblNumeroTelephone
            // 
            this.m_lblNumeroTelephone.AutoSize = true;
            this.m_lblNumeroTelephone.Location = new System.Drawing.Point(9, 58);
            this.m_lblNumeroTelephone.Name = "m_lblNumeroTelephone";
            this.m_lblNumeroTelephone.Size = new System.Drawing.Size(112, 13);
            this.m_lblNumeroTelephone.TabIndex = 2;
            this.m_lblNumeroTelephone.Text = "&Numéro de téléphone:";
            // 
            // m_txtNumeroTelephone
            // 
            this.m_txtNumeroTelephone.Location = new System.Drawing.Point(12, 74);
            this.m_txtNumeroTelephone.Name = "m_txtNumeroTelephone";
            this.m_txtNumeroTelephone.Size = new System.Drawing.Size(179, 20);
            this.m_txtNumeroTelephone.TabIndex = 3;
            // 
            // m_btnOK
            // 
            this.m_btnOK.Location = new System.Drawing.Point(102, 110);
            this.m_btnOK.Name = "m_btnOK";
            this.m_btnOK.Size = new System.Drawing.Size(75, 23);
            this.m_btnOK.TabIndex = 4;
            this.m_btnOK.Text = "&OK";
            this.m_btnOK.UseVisualStyleBackColor = true;
            this.m_btnOK.Click += new System.EventHandler(this.m_btnOK_Click);
            // 
            // m_btnAnnuler
            // 
            this.m_btnAnnuler.Location = new System.Drawing.Point(183, 110);
            this.m_btnAnnuler.Name = "m_btnAnnuler";
            this.m_btnAnnuler.Size = new System.Drawing.Size(75, 23);
            this.m_btnAnnuler.TabIndex = 5;
            this.m_btnAnnuler.Text = "A&nnuler";
            this.m_btnAnnuler.UseVisualStyleBackColor = true;
            // 
            // FenetreSuccursale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 144);
            this.Controls.Add(this.m_btnAnnuler);
            this.Controls.Add(this.m_btnOK);
            this.Controls.Add(this.m_txtNumeroTelephone);
            this.Controls.Add(this.m_lblNumeroTelephone);
            this.Controls.Add(this.m_txtAdresse);
            this.Controls.Add(this.m_lblAdresse);
            this.Name = "FenetreSuccursale";
            this.Text = "fenetressuccurale";
            this.Load += new System.EventHandler(this.FenetreSuccursale_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label m_lblAdresse;
        private System.Windows.Forms.TextBox m_txtAdresse;
        private System.Windows.Forms.Label m_lblNumeroTelephone;
        private System.Windows.Forms.TextBox m_txtNumeroTelephone;
        private System.Windows.Forms.Button m_btnOK;
        private System.Windows.Forms.Button m_btnAnnuler;
    }
}