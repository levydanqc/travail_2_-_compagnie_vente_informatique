﻿/******************************************************************************
 * Fichier      : Produit.cs
 * 
 * Classe       : Produit
 *
 * Description  : Classe d'un produit.
 * 
 * Auteur       : Dan Lévy
 * 
 * GitLab       : gitlab.com/levydanqc/travail_2_-_compagnie_vente_informatique
 ******************************************************************************/

using System;

namespace Commerce
{
    /// <summary>
    /// Énumération des catégories de produits.
    /// </summary>
    public enum Categorie
    {
        Processeurs,
        Memoires,
        UnitesStockage,
        CartesVideo,
        BlocsAlimentation,
        Boitiers
    }

    /// <summary>
    /// Classe représentant un produit.
    /// </summary>
    public class Produit
    {
        private string m_strCode;
        private string m_strDescription;
        private Categorie m_laCategorie;
        private decimal m_mCout;

        /// <summary>
        /// Constructeur de la classe produit.
        /// </summary>
        /// <param name="strCode">Le code.</param>
        /// <param name="strDescription">La description.</param>
        /// <param name="laCategorie">La catégorie.</param>
        /// <param name="mCout">Le coût d'achat.</param>
        public Produit(string strCode, string strDescription,
            Categorie laCategorie, decimal mCout)
        {
            if (strCode == "")
            {
                throw new ArgumentException("Le code est vide.");
            }
            this.m_strCode = strCode;
            this.Description = strDescription;
            this.m_laCategorie = laCategorie;
            this.Cout = mCout;
        }

        /// <summary>
        /// Accesseur en lecture du code.
        /// </summary>
        public string Code
        {
            get { return m_strCode; }
        }

        /// <summary>
        /// Accesseur en lecture et en écriture de la description.
        /// </summary>
        public string Description
        {
            get { return m_strDescription; }
            set
            {
                if (value == "")
                {
                    throw new ArgumentException("La description est vide.");
                }
                m_strDescription = value;
            }
        }

        /// <summary>
        /// Accesseur en lecture et en écriture de la catégorie.
        /// </summary>
        public Categorie Categorie
        {
            get { return m_laCategorie; }
            set { m_laCategorie = value; }
        }

        /// <summary>
        /// Accesseur en lecture et en écriture du coût d'achat.
        /// </summary>
        public decimal Cout
        {
            get { return m_mCout; }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException
                        ("Le coût n'est pas supérieur à zéro.");
                }
                m_mCout = value;
            }
        }

        /// <summary>
        /// Réécriture de la méthode ToString()
        /// </summary>
        /// <returns>Une chaine de caractères contenant le code et la
        /// descrition du produit.</returns>
        public override string ToString()
        {
            return String.Format("{0} - {1}", m_strCode, m_strDescription);
        }

        /// <summary>
        /// Réécriture de l'opérateur d'égalité.
        /// </summary>
        /// <param name="produit1">Un produit à comparer.</param>
        /// <param name="produit2">Un produit à comparer.</param>
        /// <returns>true si les codes des produits sont identiques,
        /// false autrement.</returns>
        public static bool operator == (in Produit produit1,
            in Produit produit2)
        {
            if (Object.ReferenceEquals(produit1, produit2))
            {
                return true;
            }
            if ((Object)produit1 == null
               || (Object)produit2 == null)
            {
                return false;
            }
            return (produit1.m_strCode == produit2.m_strCode);
        }

        /// <summary>
        /// Réécriture de l'opérateur d'inégalité.
        /// </summary>
        /// <param name="produit1">Un produit à comparer.</param>
        /// <param name="produit2">Un produit à comparer.</param>
        /// <returns>true si les codes des produits sont différents,
        /// false autrement.</returns>
        public static bool operator != (in Produit produit1,
            in Produit produit2)
        {
            return !(produit1 == produit2);
        }

        public override bool Equals(object obj)
        {
            if (obj is Produit)
            {
                return (this == (Produit)obj);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
