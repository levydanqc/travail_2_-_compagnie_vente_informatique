﻿using System;

namespace Commerce
{
    partial class FenetrePrincipale
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose (bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose ();
            }
            base.Dispose (disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent ()
        {
            this.m_lblSuccursales = new System.Windows.Forms.Label();
            this.m_lstSuccursales = new System.Windows.Forms.ListBox();
            this.m_lblProduits = new System.Windows.Forms.Label();
            this.m_lstProduits = new System.Windows.Forms.ListBox();
            this.m_btnAjouterSuccursale = new System.Windows.Forms.Button();
            this.m_btnModifierSuccursale = new System.Windows.Forms.Button();
            this.m_btnSupprimerSuccursale = new System.Windows.Forms.Button();
            this.m_btnAjouterProduit = new System.Windows.Forms.Button();
            this.m_btnModifierProduit = new System.Windows.Forms.Button();
            this.m_btnSupprimerProduit = new System.Windows.Forms.Button();
            this.m_btnGenererVentes = new System.Windows.Forms.Button();
            this.m_btnCommanderProduits = new System.Windows.Forms.Button();
            this.m_btnAPropos = new System.Windows.Forms.Button();
            this.m_btnBilan = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_lblSuccursales
            // 
            this.m_lblSuccursales.AutoSize = true;
            this.m_lblSuccursales.Location = new System.Drawing.Point(12, 9);
            this.m_lblSuccursales.Name = "m_lblSuccursales";
            this.m_lblSuccursales.Size = new System.Drawing.Size(68, 13);
            this.m_lblSuccursales.TabIndex = 0;
            this.m_lblSuccursales.Text = "&Succursales:";
            // 
            // m_lstSuccursales
            // 
            this.m_lstSuccursales.FormattingEnabled = true;
            this.m_lstSuccursales.Location = new System.Drawing.Point(15, 25);
            this.m_lstSuccursales.Name = "m_lstSuccursales";
            this.m_lstSuccursales.Size = new System.Drawing.Size(228, 290);
            this.m_lstSuccursales.TabIndex = 1;
            this.m_lstSuccursales.MouseClick += new System.Windows.Forms.MouseEventHandler(this.m_lstSuccursales_MouseClick);
            this.m_lstSuccursales.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.m_lstSuccursales_MouseDoubleClick);
            // 
            // m_lblProduits
            // 
            this.m_lblProduits.AutoSize = true;
            this.m_lblProduits.Location = new System.Drawing.Point(256, 9);
            this.m_lblProduits.Name = "m_lblProduits";
            this.m_lblProduits.Size = new System.Drawing.Size(48, 13);
            this.m_lblProduits.TabIndex = 2;
            this.m_lblProduits.Text = "&Produits:";
            // 
            // m_lstProduits
            // 
            this.m_lstProduits.FormattingEnabled = true;
            this.m_lstProduits.Location = new System.Drawing.Point(259, 25);
            this.m_lstProduits.Name = "m_lstProduits";
            this.m_lstProduits.Size = new System.Drawing.Size(228, 290);
            this.m_lstProduits.TabIndex = 3;
            this.m_lstProduits.MouseClick += new System.Windows.Forms.MouseEventHandler(this.m_lstProduits_MouseClick);
            this.m_lstProduits.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.m_lstProduits_MouseDoubleClick);
            // 
            // m_btnAjouterSuccursale
            // 
            this.m_btnAjouterSuccursale.Location = new System.Drawing.Point(504, 25);
            this.m_btnAjouterSuccursale.Name = "m_btnAjouterSuccursale";
            this.m_btnAjouterSuccursale.Size = new System.Drawing.Size(130, 23);
            this.m_btnAjouterSuccursale.TabIndex = 4;
            this.m_btnAjouterSuccursale.Text = "Ajouter s&uccursale";
            this.m_btnAjouterSuccursale.UseVisualStyleBackColor = true;
            this.m_btnAjouterSuccursale.Click += new System.EventHandler(this.m_btnAjouterSuccursale_Click);
            // 
            // m_btnModifierSuccursale
            // 
            this.m_btnModifierSuccursale.Location = new System.Drawing.Point(504, 54);
            this.m_btnModifierSuccursale.Name = "m_btnModifierSuccursale";
            this.m_btnModifierSuccursale.Size = new System.Drawing.Size(130, 23);
            this.m_btnModifierSuccursale.TabIndex = 5;
            this.m_btnModifierSuccursale.Text = "Modifier su&ccursale";
            this.m_btnModifierSuccursale.UseVisualStyleBackColor = true;
            this.m_btnModifierSuccursale.Click += new System.EventHandler(this.m_btnModifierSuccursale_Click);
            // 
            // m_btnSupprimerSuccursale
            // 
            this.m_btnSupprimerSuccursale.Location = new System.Drawing.Point(504, 83);
            this.m_btnSupprimerSuccursale.Name = "m_btnSupprimerSuccursale";
            this.m_btnSupprimerSuccursale.Size = new System.Drawing.Size(130, 23);
            this.m_btnSupprimerSuccursale.TabIndex = 6;
            this.m_btnSupprimerSuccursale.Text = "Supprimer succu&rsale";
            this.m_btnSupprimerSuccursale.UseVisualStyleBackColor = true;
            this.m_btnSupprimerSuccursale.Click += new System.EventHandler(this.m_btnSupprimerSuccursale_Click);
            // 
            // m_btnAjouterProduit
            // 
            this.m_btnAjouterProduit.Location = new System.Drawing.Point(504, 112);
            this.m_btnAjouterProduit.Name = "m_btnAjouterProduit";
            this.m_btnAjouterProduit.Size = new System.Drawing.Size(130, 23);
            this.m_btnAjouterProduit.TabIndex = 7;
            this.m_btnAjouterProduit.Text = "Ajouter pr&oduit";
            this.m_btnAjouterProduit.UseVisualStyleBackColor = true;
            this.m_btnAjouterProduit.Click += new System.EventHandler(this.m_btnAjouterProduit_Click);
            // 
            // m_btnModifierProduit
            // 
            this.m_btnModifierProduit.Location = new System.Drawing.Point(504, 141);
            this.m_btnModifierProduit.Name = "m_btnModifierProduit";
            this.m_btnModifierProduit.Size = new System.Drawing.Size(130, 23);
            this.m_btnModifierProduit.TabIndex = 8;
            this.m_btnModifierProduit.Text = "Modifier pro&duit";
            this.m_btnModifierProduit.UseVisualStyleBackColor = true;
            this.m_btnModifierProduit.Click += new System.EventHandler(this.m_btnModifierProduit_Click);
            // 
            // m_btnSupprimerProduit
            // 
            this.m_btnSupprimerProduit.Location = new System.Drawing.Point(504, 170);
            this.m_btnSupprimerProduit.Name = "m_btnSupprimerProduit";
            this.m_btnSupprimerProduit.Size = new System.Drawing.Size(130, 23);
            this.m_btnSupprimerProduit.TabIndex = 9;
            this.m_btnSupprimerProduit.Text = "Supprimer produ&it";
            this.m_btnSupprimerProduit.UseVisualStyleBackColor = true;
            this.m_btnSupprimerProduit.Click += new System.EventHandler(this.m_btnSupprimerProduit_Click);
            // 
            // m_btnGenererVentes
            // 
            this.m_btnGenererVentes.Location = new System.Drawing.Point(504, 228);
            this.m_btnGenererVentes.Name = "m_btnGenererVentes";
            this.m_btnGenererVentes.Size = new System.Drawing.Size(130, 23);
            this.m_btnGenererVentes.TabIndex = 11;
            this.m_btnGenererVentes.Text = "&Générer ventes";
            this.m_btnGenererVentes.UseVisualStyleBackColor = true;
            this.m_btnGenererVentes.Click += new System.EventHandler(this.m_btnGenererVentes_Click);
            // 
            // m_btnCommanderProduits
            // 
            this.m_btnCommanderProduits.Location = new System.Drawing.Point(504, 199);
            this.m_btnCommanderProduits.Name = "m_btnCommanderProduits";
            this.m_btnCommanderProduits.Size = new System.Drawing.Size(130, 23);
            this.m_btnCommanderProduits.TabIndex = 10;
            this.m_btnCommanderProduits.Text = "Co&mmander produits";
            this.m_btnCommanderProduits.UseVisualStyleBackColor = true;
            this.m_btnCommanderProduits.Click += new System.EventHandler(this.m_btnCommanderProduits_Click);
            // 
            // m_btnAPropos
            // 
            this.m_btnAPropos.Location = new System.Drawing.Point(504, 286);
            this.m_btnAPropos.Name = "m_btnAPropos";
            this.m_btnAPropos.Size = new System.Drawing.Size(130, 23);
            this.m_btnAPropos.TabIndex = 13;
            this.m_btnAPropos.Text = "À propos d&e";
            this.m_btnAPropos.UseVisualStyleBackColor = true;
            this.m_btnAPropos.Click += new System.EventHandler(this.m_btnAPropos_Click);
            // 
            // m_btnBilan
            // 
            this.m_btnBilan.Location = new System.Drawing.Point(504, 257);
            this.m_btnBilan.Name = "m_btnBilan";
            this.m_btnBilan.Size = new System.Drawing.Size(130, 23);
            this.m_btnBilan.TabIndex = 12;
            this.m_btnBilan.Text = "&Bilan";
            this.m_btnBilan.UseVisualStyleBackColor = true;
            this.m_btnBilan.Click += new System.EventHandler(this.m_btnBilan_Click);
            // 
            // FenetrePrincipale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 327);
            this.Controls.Add(this.m_btnBilan);
            this.Controls.Add(this.m_btnAPropos);
            this.Controls.Add(this.m_btnCommanderProduits);
            this.Controls.Add(this.m_btnGenererVentes);
            this.Controls.Add(this.m_btnSupprimerProduit);
            this.Controls.Add(this.m_btnModifierProduit);
            this.Controls.Add(this.m_btnAjouterProduit);
            this.Controls.Add(this.m_btnSupprimerSuccursale);
            this.Controls.Add(this.m_btnModifierSuccursale);
            this.Controls.Add(this.m_btnAjouterSuccursale);
            this.Controls.Add(this.m_lstProduits);
            this.Controls.Add(this.m_lblProduits);
            this.Controls.Add(this.m_lstSuccursales);
            this.Controls.Add(this.m_lblSuccursales);
            this.Name = "FenetrePrincipale";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FenetrePrincipale_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label m_lblSuccursales;
        private System.Windows.Forms.ListBox m_lstSuccursales;
        private System.Windows.Forms.Label m_lblProduits;
        private System.Windows.Forms.ListBox m_lstProduits;
        private System.Windows.Forms.Button m_btnAjouterSuccursale;
        private System.Windows.Forms.Button m_btnModifierSuccursale;
        private System.Windows.Forms.Button m_btnSupprimerSuccursale;
        private System.Windows.Forms.Button m_btnAjouterProduit;
        private System.Windows.Forms.Button m_btnModifierProduit;
        private System.Windows.Forms.Button m_btnSupprimerProduit;
        private System.Windows.Forms.Button m_btnGenererVentes;
        private System.Windows.Forms.Button m_btnCommanderProduits;
        private System.Windows.Forms.Button m_btnBilan;
        private System.Windows.Forms.Button m_btnAPropos;
    }
}

