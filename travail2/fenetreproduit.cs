﻿/******************************************************************************
 * Fichier      : fenetreproduit.cs
 * 
 * Classe       : FenetreProduit (dérivé de Form)
 *
 * Description  : Fenêtre d'ajout et de modification d'un produit.
 * 
 * Auteur       : Dan Lévy
 * 
 * GitLab       : gitlab.com/levydanqc/travail_2_-_compagnie_vente_informatique
 ******************************************************************************/

using System.Windows.Forms;
using System;

namespace Commerce
{
    /// <summary>
    /// Classe représentant une fenêtre d'ajout d'un produit.
    /// </summary>
    public partial class FenetreProduit : Form
    {
        // Produit à ajouter ou à modifier
        private Produit m_leProduit;

        /// <summary>
        /// Constructeur d'une fenêtre d'ajout d'un produit.
        /// </summary>
        public FenetreProduit()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MinimizeBox = false;
            MaximizeBox = false;
            ShowInTaskbar = false;
            AcceptButton = m_btnOK;
            CancelButton = m_btnAnnuler;
            m_btnOK.DialogResult = DialogResult.OK;
            m_btnAnnuler.DialogResult = DialogResult.Cancel;
            m_txtCode.Enabled = false;
            m_optProcesseurs.Checked = true;
        }

        private void FenetreProduit_Load(object sender, System.EventArgs e)
        {

        }

        /// <summary>
        /// Accesseur en lecture et en écriture d'un produit.
        /// </summary>
        public Produit LeProduit
        {
            get { return m_leProduit; }
            set
            {
                m_leProduit = value;
                m_txtCode.Text = value.Code;
                m_txtCout.Text = value.Cout.ToString();
                m_txtDescription.Text = value.Description;
            }
        }

        /// <summary>
        /// Accesseur en écriture du code du produit.
        /// </summary>
        public int LeCode
        {
            set { m_txtCode.Text = value.ToString(); }
        }

        /// <summary>
        /// Bouton OK.
        /// Permet de créer ou de modifier un produit.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_btnOK_Click(object sender, System.EventArgs e)
        {
            Categorie laCategorie;
            try
            {
                if (m_optProcesseurs.Checked)
                {
                    laCategorie = Categorie.Processeurs;
                }
                else if (m_optMemoires.Checked)
                {
                    laCategorie = Categorie.Memoires;
                }
                else if (m_optUnitesStockage.Checked)
                {
                    laCategorie = Categorie.UnitesStockage;
                }
                else if (m_optCartesVideo.Checked)
                {
                    laCategorie = Categorie.CartesVideo;
                }
                else if (m_optBlocsAlimentation.Checked)
                {
                    laCategorie = Categorie.BlocsAlimentation;
                }
                else
                {
                    laCategorie = Categorie.Boitiers;
                }
                this.LeProduit = new Produit(
                    m_txtCode.Text,
                    m_txtDescription.Text,
                    laCategorie,
                    decimal.Parse(m_txtCout.Text));
            }
            catch (ArgumentException uneException)
            {
                MessageBox.Show(
                    uneException.Message,
                    FenetrePrincipale.TITRE_APPLICATION,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);

                if (uneException.Message == "La description est vide.")
                {
                    m_txtDescription.Focus();
                }
                else
                {
                    m_txtCout.Focus();
                }

                DialogResult = DialogResult.None;
            }
            catch (FormatException)
            {
                MessageBox.Show(
                    "Le coût du produit ne peut ni être vide ni contenir un " +
                    "caractère autre qu'un chiffre.",
                    FenetrePrincipale.TITRE_APPLICATION,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);

                m_txtCout.Focus();

                DialogResult = DialogResult.None;
            }
        }

    }
}
