﻿/******************************************************************************
 * Fichier      : Stock.cs
 * 
 * Classe       : Stock
 *
 * Description  : Classe d'un stock d'un produit dans une succursale.
 * 
 * Auteur       : Dan Lévy
 * 
 * GitLab       : gitlab.com/levydanqc/travail_2_-_compagnie_vente_informatique
 ******************************************************************************/

using System;

namespace Commerce
{
    /// <summary>
    /// Classe représentant un stock d'un produit et d'une succursale.
    /// </summary>
    public class Stock
    {
        private Succursale m_laSuccursale;
        private Produit m_leProduit;
        private uint m_uiQuantite;

        /// <summary>
        /// Constructeur d'un stock.
        /// </summary>
        /// <param name="laSuccursale">Une succursale.</param>
        /// <param name="leProduit">Un produit.</param>
        /// <param name="uiQuantite">La quantité du produit dans la 
        /// succursale.</param>
        public Stock(in Succursale laSuccursale, in Produit leProduit)
        {
            if ((Object)laSuccursale == null)
            {
                throw new ArgumentNullException(null, "La succursale est nulle.");
            }
            if ((Object)leProduit == null)
            {
                throw new ArgumentNullException(null, "Le produit est nul.");
            }
            this.m_laSuccursale = laSuccursale;
            this.m_leProduit = leProduit;
        }

        /// <summary>
        /// Accesseur en lecture de la succursale.
        /// </summary>
        public Succursale Succursale
        {
            get { return m_laSuccursale; }
        }

        /// <summary>
        /// Accesseur en lecture du produit.
        /// </summary>
        public Produit Produit
        {
            get { return m_leProduit; }
        }

        /// <summary>
        /// Accesseur en lecture et en écriture de la quantité.
        /// </summary>
        public uint Quantite
        {
            get { return m_uiQuantite; }
            set { m_uiQuantite = value; }
        }

        /// <summary>
        /// Réécriture de l'opérateur d'égalité.
        /// </summary>
        /// <param name="stock1">un stock à comparer.</param>
        /// <param name="stock2">un stock à comparer.</param>
        /// <returns>true si les succursales et les produits sont identiques,
        /// false autrement.</returns>
        public static bool operator == (in Stock stock1, in Stock stock2)
        {
            if (Object.ReferenceEquals(stock1, stock2))
            {
                return true;
            }
            if ((Object)stock1 == null
               || (Object)stock2 == null)
            {
                return false;
            }
            return (stock1.m_laSuccursale == stock2.m_laSuccursale
                && stock1.m_leProduit == stock2.m_leProduit);
        }

        /// <summary>
        /// Réécriture de l'opérateur d'inégalité.
        /// </summary>
        /// <param name="stock1">un stock à comparer.</param>
        /// <param name="stock2">un stock à comparer.</param>
        /// <returns>true si les succursales et les produits sont différents,
        /// false autrement.</returns>
        public static bool operator != (in Stock produit1, in Stock produit2)
        {
            return !(produit1 == produit2);
        }

        public override bool Equals(object obj)
        {
            if (obj is Stock)
            {
                return (this == (Stock)obj);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
