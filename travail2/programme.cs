﻿/******************************************************************************
 * Fichier      : programme.cs
 * 
 * Classe       : Programme
 *
 * Description  : Point de départ du programme.
 * 
 * Auteur       : Dan Lévy
 * 
 * GitLab       : gitlab.com/levydanqc/travail_2_-_compagnie_vente_informatique
 ******************************************************************************/

using System;
using System.Windows.Forms;
using Commerce;
// TODO : Commenter chaque fonction
namespace travail2
{
    public static class Programme
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FenetrePrincipale());
        }
    }
}
